using Microsoft.AspNetCore.Mvc;

namespace ApiWeb.Greetings
{
    [Route("greeting")]
    public class GreetingController : Controller
    {
        [HttpGet]
        public IActionResult Get()
        {
            var builder = new GreetingBuilder();

            return Ok(builder.Build());
        }
    }
}