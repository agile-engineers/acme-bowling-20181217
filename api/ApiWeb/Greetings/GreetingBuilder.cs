namespace ApiWeb.Greetings
{
    public class GreetingBuilder
    {
        public Greeting Build(string name = "World")
        {
            return new Greeting {Text = $"Hello, { name }!"};
        }
    }
}