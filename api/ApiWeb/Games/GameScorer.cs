using System.Collections.Generic;
using System.Linq;

namespace ApiWeb.Games
{
    public static class GameScorer
    {
        public static IEnumerable<int> Calculate(string symbols)
        {
            return CalculateFrames(TranslateSymbols(symbols));
        }

        private static List<int> CalculateFrames(List<int> rolls)
        {
            var frames = new List<int>();
            var rollIndex = 0;
            var score = 0;

            for (var frame = 0; frame < 10; frame++)
            {
                var firstRoll = rolls[rollIndex];
                var secondRoll = rolls[rollIndex + 1];

                var isStrike = firstRoll == 10;
                var isSpare = !isStrike && firstRoll + secondRoll == 10;

                var bonus = isStrike || isSpare ? rolls[rollIndex + 2] : 0;

                score += firstRoll + secondRoll + bonus;
                rollIndex += isStrike ? 1 : 2;

                frames.Add(score);
            }

            return frames;
        }

        private static List<int> TranslateSymbols(string symbols)
        {
            var rolls = new List<int>();

            foreach (char s in symbols)
            {
                var roll =
                    s == '-' ? 0 :
                    s == '/' ? 10 - rolls.Last() :
                    s == 'X' ? 10 : (int) char.GetNumericValue(s);

                rolls.Add(roll);
            }

            return rolls;
        }
    }
}