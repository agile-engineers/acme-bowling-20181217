using System;
using System.Net.Http;
using Newtonsoft.Json;

namespace ApiWeb.Games
{
    public class HttpUniversalGameGateway : IUniversalBowlingGateway
    {
        public Game Fetch(string code)
        {
            var url = $"https://grn4aewhhg.execute-api.us-east-1.amazonaws.com/prod/games/{code}";
            var client = new HttpClient();
            var responseText = client.GetStringAsync(url).Result;

            var game = JsonConvert.DeserializeObject<Game>(responseText);

            if (game.Date == null)
            {
                throw new ArgumentException("ubc-unknown");
            }
            else
            {
                return game;
            }
        }
    }
}