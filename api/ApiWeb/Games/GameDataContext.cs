using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace ApiWeb.Games
{
    public class GameData
    {
        public string Id { get; set; }
        public string Date { get; set; }
        public string Rolls { get; set; }
        public IEnumerable<int> Frames => GameScorer.Calculate(this.Rolls);
    }

    public class GameDataContext : DbContext
    {
        public GameDataContext()
        {
        }

        public GameDataContext(DbContextOptions<GameDataContext> options)
            : base(options)
        {
        }

        public DbSet<GameData> Games { get; set; }
    }
}