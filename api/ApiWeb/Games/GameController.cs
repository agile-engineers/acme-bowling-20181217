using System;
using Microsoft.AspNetCore.Mvc;

namespace ApiWeb.Games
{
    [Route("games")]
    public class GamesController : Controller
    {
        private readonly GameDataContext _dataContext;

        public GamesController(GameDataContext dataContext)
        {
            _dataContext = dataContext;
        }

        [HttpGet("history")]
        public IActionResult History()
        {
            return Ok(new {IsError = false, Data = _dataContext.Games});
        }

        [HttpGet("ubs/{ubc}")]
        public IActionResult UbsGameByCode(string ubc)
        {
            var universalGateway = new HttpUniversalGameGateway();
            var gameService = new GameService(universalGateway, _dataContext);

            try
            {
                return Ok(new {IsError = false, Data = gameService.GetGame(ubc)});
            }
            catch (ArgumentException exception)
            {
                return Ok(new {IsError = true, Data = exception.Message});
            }
        }

        [HttpPost("add-game")]
        public IActionResult AddGame([FromBody] GameData game)
        {
            _dataContext.Games.Add(game);
            _dataContext.SaveChanges();
            return Ok(new {IsError = false, Data = "game-added"});
        }
    }
}