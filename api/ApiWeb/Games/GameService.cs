using System;
using System.Collections.Generic;
using System.Linq;

namespace ApiWeb.Games
{
    public class Player
    {
        public string Name { get; set; }
        public string Rolls { get; set; }

        public IEnumerable<int> Frames => GameScorer.Calculate(this.Rolls);
    }

    public class Game
    {
        public string Date { get; set; }
        public List<Player> Players { get; set; }
    }

    public interface IUniversalBowlingGateway
    {
        Game Fetch(string ubc);
    }

    public class GameService
    {
        private readonly IUniversalBowlingGateway _universalBowlingGateway;
        private readonly GameDataContext _dataContext;

        public GameService(
            IUniversalBowlingGateway universalBowlingGateway,
            GameDataContext dataContext)
        {
            _universalBowlingGateway = universalBowlingGateway;
            _dataContext = dataContext;
        }

        public Game GetGame(string ubc)
        {
            var existingGame = _dataContext.Games.SingleOrDefault(game => game.Id == ubc);
            
            if (existingGame != null)
            {
                throw new ArgumentException("ubc-already-used");
            }

            if (ubc.Length < 4 || ubc.Length > 4)
            {
                throw new ArgumentException("invalid-ubc");
            }

            try
            {
                return _universalBowlingGateway.Fetch(ubc);
            }
            catch (Exception e)
            {
                throw new ArgumentException("unknown-ubc");
            }
        }
    }
}