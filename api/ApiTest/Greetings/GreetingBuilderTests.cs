using ApiWeb.Greetings;
using Xunit;

namespace ApiTest.Greetings
{
    public class GreetingBuilderTests
    {
        [Fact]
        public void build_a_default_greeting()
        {
            GreetingBuilder sut = new GreetingBuilder();

            Greeting actual = sut.Build();

            Assert.Equal("Hello, World!", actual.Text);
        }
        
        [Fact]
        public void build_a_greeting_with_name()
        {
            GreetingBuilder sut = new GreetingBuilder();

            Greeting actual = sut.Build("joe");

            Assert.Equal("Hello, joe!", actual.Text);
        }
    }
}