using ApiWeb.Greetings;
using Moq;
using Xunit;

namespace ApiTest.Greetings
{
    public class GreetingServiceTests
    {
        [Fact]
        public void build_custom_greeting_with_name_and_language()
        {
            var translationGateway = new Mock<ITranslationGateway>();
            GreetingService sut = new GreetingService(translationGateway.Object);

            translationGateway.Setup(tg => tg.Translate("spanish")).Returns("Hola");

            Greeting result = sut.Build("Joe", "spanish");
    
            Assert.Equal("Hola, Joe!", result.Text);
        }
    }

    public class GreetingService
    {
        private readonly ITranslationGateway _translationGateway;

        public GreetingService(ITranslationGateway translationGateway)
        {
            _translationGateway = translationGateway;
        }

        public Greeting Build(string name, string language)
        {
            return new Greeting
            {
                Text = $"{_translationGateway.Translate(language)}, Joe!"
            };
        }
    }

    public interface ITranslationGateway
    {
        string Translate(string language);
    }
}