using System.Collections.Generic;
using ApiWeb.Games;
using Xunit;

namespace ApiTest.Games
{
    public class GameScorerTests
    {
        [Fact]
        public void score_standard_games()
        {
            Assert.Equal(new List<int>
            {
                0, 0, 0, 0, 0,
                0, 0, 0, 0, 0
            }, GameScorer.Calculate("--------------------"));

            Assert.Equal(new List<int>
            {
                2, 2, 2, 2, 2,
                2, 2, 2, 2, 2
            }, GameScorer.Calculate("11------------------"));

            Assert.Equal(new List<int>
            {
                2, 3, 3, 3, 3,
                3, 3, 3, 3, 3
            }, GameScorer.Calculate("111-----------------"));
        }

        [Fact]
        public void score_games_with_spares()
        {
            Assert.Equal(new List<int>
            {
                11, 12, 12, 12, 12,
                12, 12, 12, 12, 12
            }, GameScorer.Calculate("1/1-----------------"));

            Assert.Equal(new List<int>
            {
                0, 0, 0, 0, 0,
                0, 0, 0, 0, 11
            }, GameScorer.Calculate("------------------1/1"));
        }

        [Fact]
        public void score_game_with_strikes()
        {
            Assert.Equal(new List<int>
            {
                12, 14, 14, 14, 14,
                14, 14, 14, 14, 14
            }, GameScorer.Calculate("X11----------------"));

            Assert.Equal(new List<int>
            {
                0, 0, 0, 0, 0,
                0, 0, 0, 0, 12
            }, GameScorer.Calculate("------------------X11"));
        }
    }
}