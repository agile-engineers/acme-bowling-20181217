using System;
using System.Collections.Generic;
using ApiWeb.Games;
using Microsoft.EntityFrameworkCore;
using Moq;
using Xunit;

namespace ApiTest.Games
{
    public class GameServiceTests
    {
        private readonly Mock<IUniversalBowlingGateway> _gatewayMock;
        private readonly GameService _gameService;
        private readonly GameDataContext _dataContext;

        public GameServiceTests()
        {
            _gatewayMock = new Mock<IUniversalBowlingGateway>();

            var options = new DbContextOptionsBuilder<GameDataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            _dataContext = new GameDataContext(options);
            _gameService = new GameService(_gatewayMock.Object, _dataContext);
        }

        [Fact]
        public void throws_exception_when_ubc_is_invalid()
        {
            var exception = Assert.Throws<ArgumentException>(() => _gameService.GetGame("111"));

            Assert.Equal("invalid-ubc", exception.Message);
        }

        [Fact]
        public void throws_exception_when_ubc_is_unknown()
        {
            _gatewayMock
                .Setup(gateway => gateway.Fetch("1111"))
                .Throws<ArgumentException>();

            var exception = Assert.Throws<ArgumentException>(() => _gameService.GetGame("1111"));

            Assert.Equal("unknown-ubc", exception.Message);
        }

        [Fact]
        public void throws_exception_when_ubc_has_already_been_used()
        {
            _dataContext.Games.Add(new GameData {Id = "1111"});
            _dataContext.SaveChanges();
            
            var exception = Assert.Throws<ArgumentException>(() => _gameService.GetGame("1111"));

            Assert.Equal("ubc-already-used", exception.Message);
        }

        [Fact]
        public void returns_game_when_ubc_is_valid()
        {
            _gatewayMock
                .Setup(gateway => gateway.Fetch("1111"))
                .Returns(new Game
                {
                    Date = "2017-01-01",
                    Players = new List<Player>()
                    {
                        new Player {Name = "Joe", Rolls = "11111111111111111111"},
                        new Player {Name = "Jane", Rolls = "--------------------"}
                    }
                });

            Game game = _gameService.GetGame("1111");

            Assert.Equal(2, game.Players.Count);
            Assert.Equal(new List<int> {2, 4, 6, 8, 10, 12, 14, 16, 18, 20}, game.Players[0].Frames);
            Assert.Equal(new List<int> {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, game.Players[1].Frames);
        }
    }
}