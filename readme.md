# ACME Bowling

## configure git

    git config user.name "Erick Fleming"
    git config user.email "goingagile0@gmail.com"

## clone project repository

    git clone https://goingagile0@bitbucket.org/agile-engineers/acme-bowling-20181217.git

## initialize and start api server

    cd acme-bowling-20181217
    explorer .

## initialize editors

    cd acme-bowling-20181217
    code .

## initialize and start client

    cd acme-bowling-20181217\client
    npm install
    npm start

## initialize and run integrated tests

    cd acme-bowling-20181217\test
    npm install
    npm start