import { createNode } from "../utils/html"
import * as Effects from "../utils/effects"

export const init = function () {
    return [
        { message: "", history: [] },
        Effects.query( "/games/history", "display-history" )
    ]
}

export const update = function ( message, state ) {
    const { name, data } = message
    let effect = Effects.none

    switch ( name ) {
        case "display-history":
            state = { ...state, history: data.data }
            break
    }

    return [ state, effect ]
}

export const view = function ( state ) {
    const gameView = game => <tr>
        <td>{ game.date }</td>
        <td>{ game.rolls }</td>
        <td>{ game.frames[ 9 ] }</td>
    </tr>

    return <main>
        <h2>Game History</h2>
        <table>
            <thead>
                <th>Date</th>
                <th>Rolls</th>
                <th>Score</th>
            </thead>
            <tbody>{ state.history.map( gameView ) }</tbody>
        </table>
        <div><a href="../add-game.html">Add Game</a></div>
    </main>
}

