import { createNode } from "../utils/html"

import * as Effects from "../utils/effects"

export const init = function () {
    return [
        { message: "", valid: false },
        Effects.none
    ]
}

export const update = function ( message, state ) {
    let effect = Effects.none

    switch ( message.name ) {
        case "validate-credentials":
            const valid = message.data.email
                && message.data.email.length > 0
                && message.data.password
                && message.data.password.length > 0

            state = { ...state, valid }
            break
    }

    return [ state, effect ]
}

export const view = function ( state ) {
    return <main>
        <form id="login-form" oninput="validate-credentials" onsubmit="authenticate">
            <div>
                <label for="email">Email:</label>
                <input type="email" name="email" autocomplete="off" required/>
            </div>
            <div>
                <label for="password">Password:</label>
                <input type="password" name="password" autocomplete="off" required/>
            </div>
            <div>
                <button disabled={ !state.valid }>Login</button>
            </div>
        </form>
    </main>
}

