import { assert } from "chai"

import * as Main from "./main"
import FakeRuntime from "../utils/runtime-fake"

describe( "login page", function () {
    let runtime: FakeRuntime

    beforeEach( function () {
        runtime = new FakeRuntime( Main )
    } )

    it( "disables login button by default", function () {
        return runtime.start().then( function ( document ) {
            assert.isNotNull( document.attribute( "#login-form button", "disabled" ) )
        } )
    } )

    it( "enables login button when valid credentials are supplied", function () {
        runtime.populateForm( "#login-form", { email: "e@m.com", password: "pass" } )

        return runtime.start().then( function ( document ) {
            assert.isNull( document.attribute( "#login-form button", "disabled" ) )
        } )
    } )
} )
