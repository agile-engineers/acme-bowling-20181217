import { createNode } from "../utils/html"

import * as Effects from "../utils/effects"

export const init = function () {
    const state = { text: "" }
    const effect = Effects.query( "/greeting", "update-greeting" )

    return [ state, effect ]
}

export const update = function ( message, state ) {
    let effect = Effects.none

    if ( message.name === "update-greeting" ) {
        state = { ...state, text: message.data.text }
    }

    return [ state, effect ]
}

export const view = function ( state ) {
    return <div>
        <h1 id="message">{ state.text }</h1>
    </div>
}

