import { assert } from "chai"

describe( "bowling", function () {

    it( "calculates standard games", function () {
        assert.deepEqual( [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ], calculateScore( "--------------------" ) )
        assert.deepEqual( [ 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 ], calculateScore( "11------------------" ) )
        assert.deepEqual( [ 2, 3, 3, 3, 3, 3, 3, 3, 3, 3 ], calculateScore( "111-----------------" ) )
    } )

    it( "calculates spares", function () {
        assert.deepEqual( [ 11, 12, 12, 12, 12, 12, 12, 12, 12, 12 ], calculateScore( "1/1-----------------" ) )
        assert.deepEqual( [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 11 ], calculateScore( "------------------1/1" ) )
    } )

    it( "calculates strikes", function () {
        assert.deepEqual( [ 12, 14, 14, 14, 14, 14, 14, 14, 14, 14 ], calculateScore( "X11----------------" ) )
        assert.deepEqual( [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 12 ], calculateScore( "------------------X11" ) )
    } )

} )

const calculateScore = function ( symbols: string ) {
    const rolls = []

    symbols.split( "" ).forEach( function ( symbol ) {
        if ( symbol === "X" ) {
            rolls.push( 10 )
        } else if ( symbol === "-" ) {
            rolls.push( 0 )
        } else if ( symbol === "/" ) {
            rolls.push( 10 - rolls[ rolls.length - 1 ] )
        } else {
            rolls.push( parseInt( symbol ) )
        }
    } )

    const frames = []

    let score = 0
    let rolls_index = 0

    for ( let frame = 1; frame <= 10; frame++ ) {
        const first_roll = rolls[ rolls_index ]
        const second_roll = rolls[ rolls_index + 1 ]

        const is_strike = first_roll === 10
        const is_spare = !is_strike && first_roll + second_roll == 10

        score += first_roll + second_roll
        score += is_strike || is_spare ? rolls[ rolls_index + 2 ] : 0

        frames.push( score )

        rolls_index += is_strike ? 1 : 2
    }

    return frames
}
