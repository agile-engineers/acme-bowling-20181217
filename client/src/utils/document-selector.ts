export default class DocumentSelector {
    constructor ( private window: Window ) {
    }

    text ( selector ) {
        return this.window.document.querySelector( selector ).textContent
    }

    html () {
        return this.window.document.body.innerHTML
    }

    textList ( selector ) {
        const text_values = []

        this.window.document.querySelectorAll( selector ).forEach( function ( element ) {
            text_values.push( element.textContent )
        } )

        return text_values
    }

    attribute ( selector, attribute_name ) {
        return this.window.document.querySelector( selector ).getAttribute( attribute_name )
    }
}

