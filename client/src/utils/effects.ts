export const none = { name: "none" }

export const query = function ( path, action ) {
    return { name: "query", path, action }
}

export const command = function ( path, data, action ) {
    return { name: "command", path, data, action }
}