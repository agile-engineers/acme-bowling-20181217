import { JSDOM } from "jsdom"

import DOM from "./dom"
import { Component } from "./types"
import DocumentSelector from "./document-selector"

const template = `<!DOCTYPE html><head><title></title></head><body></body>`

export default class FakeRuntime {
    window: Window
    dom: DOM

    queries = {}
    events = []

    constructor ( private component: Component ) {
        this.window = new JSDOM( template, { url: "http://localhost:8080" } ).window
        this.dom = new DOM( this.window )
    }

    processEffect ( effect ) {
        let action = { name: "none", data: null }

        if ( effect.name === "query" ) {
            action.name = effect.action
            action.data = this.queries[ effect.path ]
        }

        return action
    }

    populateForm ( selector, fields ) {
        const self: FakeRuntime = this

        Object.keys( fields ).forEach( function ( field_name ) {
            self.registerInput( `${ selector } [name="${ field_name }"]`, fields[ field_name ] )
        } )
    }

    registerInput ( selector, value ) {
        const self: FakeRuntime = this

        self.events.push( function () {
            const element = self.window.document.querySelector( selector )

            element.value = value
            element.dispatchEvent( new self.window[ "Event" ]( "input", { bubbles: true } ) )
        } )
    }

    registerClick ( selector ) {
        const self: FakeRuntime = this

        self.events.push( function () {
            self.window.document.querySelector( selector )
                .dispatchEvent( new self.window[ "Event" ]( "click", { bubbles: true } ) )
        } )
    }

    registerQuery ( path, response ) {
        this.queries[ path ] = response
    }

    start (): Promise<DocumentSelector> {
        const self: FakeRuntime = this
        const document_selector = new DocumentSelector( self.window )

        return new Promise( function ( resolve ) {
            let count = self.events.length
            let global_state = null
            let global_node = null

            self.window.addEventListener( "action", function ( e: any ) {
                const [ update_state, update_effect ] = self.component.update( e.detail, global_state )
                global_state = update_state

                global_node = self.dom.patch(
                    global_node,
                    self.component.view( update_state ),
                    self.window.document.body )

                const effect_action = self.processEffect( update_effect )

                if ( effect_action.name !== "none" ) {
                    count += 1
                    self.window.dispatchEvent( new self.window[ "CustomEvent" ]( "action", {
                        detail: effect_action,
                        bubbles: true,
                        cancelable: true
                    } ) )
                }

                if ( --count == 0 ) {
                    resolve( document_selector )
                }
            } )

            const [ init_state, init_effect ] = self.component.init()
            global_state = init_state

            global_node = self.dom.patch( global_node, self.component.view( global_state ), self.window.document.body )

            if ( count === 0 ) {
                resolve( document_selector )
            } else {
                self.events.forEach( function ( fn ) {
                    fn()
                } )
            }

            const effect_action = self.processEffect( init_effect )

            if ( effect_action.name !== "none" ) {
                count += 1
                self.window.dispatchEvent( new self.window[ "CustomEvent" ]( "action", {
                    detail: effect_action,
                    bubbles: true,
                    cancelable: true
                } ) )
            }
        } )
    }
}

