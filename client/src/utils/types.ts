export interface Component {
    init ()

    update ( message: any, state: any )

    view ( state: any )
}
