const DEFAULT = 0
const TEXT_NODE = 2

const EMPTY_OBJECT = {}
const EMPTY_ARRAY = []

const createVNode = function ( name, props, children, element, key, type ) {
    return { name, props, children, element, key, type }
}

const createTextVNode = function ( text, element? ) {
    return createVNode( text, EMPTY_OBJECT, EMPTY_ARRAY, element, null, TEXT_NODE )
}

export var createNode = function ( name, props ) {
    var node
    var rest = []
    var children = []
    var length = arguments.length

    while ( length-- > 2 ) rest.push( arguments[ length ] )

    if ( ( props = props == null ? {} : props ).children != null ) {
        if ( rest.length <= 0 ) {
            rest.push( props.children )
        }
        delete props.children
    }

    while ( rest.length > 0 ) {
        if ( Array.isArray( ( node = rest.pop() ) ) ) {
            for ( length = node.length; length-- > 0; ) {
                rest.push( node[ length ] )
            }
        } else if ( node === false || node === true || node == null ) {
        } else {
            children.push( typeof node === "object" ? node : createTextVNode( node ) )
        }
    }

    return typeof name === "function"
        ? name( props, ( props.children = children ) )
        : createVNode( name, props, children, null, props.key, DEFAULT )
}
