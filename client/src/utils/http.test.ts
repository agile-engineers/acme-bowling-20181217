import { assert } from "chai"

import HTTP from "./http"
import { Fetcher } from "./types"

const base = "http://localhost:8081"

describe( "http", function () {

    const fakeFetcher: Fetcher = {
        fetch: function ( url: string, options?: RequestInit ) {
            let response = {}

            if ( url === "http://localhost:8081/known-json" ) {
                response = {
                    ok: true,
                    statusText: "Ok",
                    headers: { get: () => "application/json" },
                    json: () => Promise.resolve( { name: "value" } )
                }
            } else if ( url === "http://localhost:8081/known-not-json" ) {
                response = {
                    ok: true,
                    statusText: "Ok",
                    headers: { get: () => "text/plain" }
                }
            } else {
                response = {
                    ok: false,
                    statusText: "unknown",
                    headers: {},
                }

            }

            return Promise.resolve( response as Response )
        }
    }

    const http = new HTTP( fakeFetcher, base )

    it( "returns an error when path is unknown", function () {
        return http.get( "/unknown" ).then( function ( data ) {
            assert.deepEqual( data, { error: "unknown" } )
        } )
    } )

    it( "returns de-serialized data when path is known", function () {
        return http.get( "/known-json" ).then( function ( data ) {
            assert.deepEqual( data, { name: "value" } )
        } )
    } )

    it( "throws an error if content type is not json", function () {
        return http.get( "/known-not-json" )
            .then( () => assert.fail() )
            .catch( error => assert.equal( error.message, "Server did not provide a json content type" ) )
    } )

} )