import { Component } from "./types"

import DOM from "../utils/dom"
import HTTP from "./http"

let global_state = {}
let global_node = null

export const start = function ( window: Window, component: Component ) {
    const dom = new DOM( window )
    const http = new HTTP( window, "http://localhost:8081" )

    const dispatchAction = function ( action ) {
        window.dispatchEvent(
            new window[ "CustomEvent" ]( "action", {
                detail: action,
                bubbles: true,
                cancelable: true
            } )
        )
    }

    const processEffect = function ( effect ) {
        if ( effect.name === "query" ) {
            http.get( effect.path ).then( function ( data ) {
                dispatchAction( { name: effect.action, data } )
            } )
        } else if ( effect.name === "command" ) {
            http.post( effect.path, effect.data ).then( function ( data ) {
                dispatchAction( { name: effect.action, data } )
            } )
        } else {
            console.log( "unknown effect" )
        }

    }

    window.addEventListener( "submit", function ( e ) {
        return e.preventDefault()
    } )

    window.addEventListener( "action", function ( e: CustomEvent ) {
        console.group( e.detail.name )
        console.log( e.detail.data )

        const [ update_state, update_effect ] = component.update( e.detail, global_state )

        global_state = update_state
        global_node = dom.patch( global_node, component.view( global_state ), window.document.body )

        console.log( global_state )
        console.groupEnd()

        if ( update_effect.name !== "none" ) {
            processEffect( update_effect )
        }
    } )

    let [ init_state, init_effect ] = component.init()

    global_state = init_state
    global_node = dom.patch( global_node, component.view( global_state ), window.document.body )

    processEffect( init_effect )
}
