const RECYCLED_NODE = 1
const TEXT_NODE = 2

const XLINK_NS = "http://www.w3.org/1999/xlink"
const SVG_NS = "http://www.w3.org/2000/svg"

const merge = function ( a, b ) {
    const target = {}

    for ( let i in a ) {
        target[ i ] = a[ i ]
    }

    for ( let i in b ) {
        target[ i ] = b[ i ]
    }

    return target
}

const eventProxy = function ( event ) {
    return event.currentTarget.events[ event.type ]( event )
}

const getKey = function ( node ) {
    return node == null ? null : node.key
}

const createKeyMap = function ( children, start, end ) {
    const out = {}
    let key
    let node

    for ( ; start <= end; start++ ) {
        if ( ( key = ( node = children[ start ] ).key ) != null ) {
            out[ key ] = node
        }
    }

    return out
}

const extractFormData = function ( form: HTMLFormElement ) {
    const fields: any = {}
    const elements: any = form.elements

    for ( let i = 0; i < elements.length; i++ ) {
        const element = elements[ i ]

        const is_text_box_like = element.type === "text"
            || element.type === "password"
            || element.type === "textarea"
            || element.type === "email"

        if ( is_text_box_like ) {
            fields[ element.name ] = element.value
        } else if ( element.type === "radio" && fields[ element.name ] == undefined ) {
            fields[ element.name ] = element.checked ? element.value : null
        } else if ( element.type === "checkbox" ) {
            fields[ element.name ] = element.checked
        } else if ( element.nodeName === "SELECT" ) {
            fields[ element.name ] = element.value
        } else if ( element.nodeName === "hidden" ) {
            fields[ element.name ] = element.value
        }
    }

    return fields
}

export default class DOM {
    constructor ( private window: Window ) {
    }

    updateElement ( element, lastProps, nextProps, lifecycle, isSvg, isRecycled ) {
        const self: DOM = this

        for ( let name in merge( lastProps, nextProps ) ) {
            if (
                ( name === "value" || name === "checked"
                    ? element[ name ]
                    : lastProps[ name ] ) !== nextProps[ name ]
            ) {
                self.updateProperty( element, name, lastProps[ name ], nextProps[ name ], isSvg )
            }
        }

        const cb = isRecycled ? nextProps.oncreate : nextProps.onupdate

        if ( cb != null ) {
            lifecycle.push( function () {
                cb( element, lastProps )
            } )
        }
    }

    updateProperty ( element, name, lastValue, nextValue, isSvg ) {
        const self: DOM = this

        if ( name === "key" ) {
        } else if ( name === "style" ) {
            for ( let i in merge( lastValue, nextValue ) ) {
                const style = nextValue == null || nextValue[ i ] == null ? "" : nextValue[ i ]

                if ( i[ 0 ] === "-" ) {
                    element[ name ].setProperty( i, style )
                } else {
                    element[ name ][ i ] = style
                }
            }
        } else {
            if ( name[ 0 ] === "o" && name[ 1 ] === "n" ) {
                if ( !element.events ) element.events = {}

                element.events[ ( name = name.slice( 2 ) ) ] = function ( event: Event ) {
                    let data = null
                    let currentTarget = event.currentTarget as any

                    if ( currentTarget.tagName === "FORM" ) {
                        data = extractFormData( currentTarget )
                    }

                    currentTarget.dispatchEvent(
                        new self.window[ "CustomEvent" ]( "action", {
                            detail: { name: nextValue, data, event },
                            bubbles: true,
                            cancelable: true
                        } )
                    )
                }

                if ( nextValue == null ) {
                    element.removeEventListener( name, eventProxy )
                } else if ( lastValue == null ) {
                    element.addEventListener( name, eventProxy )
                }
            } else {
                const nullOrFalse = nextValue == null || nextValue === false

                if (
                    name in element &&
                    name !== "list" &&
                    name !== "draggable" &&
                    name !== "spellcheck" &&
                    name !== "translate" &&
                    !isSvg
                ) {
                    element[ name ] = nextValue == null ? "" : nextValue
                    if ( nullOrFalse ) {
                        element.removeAttribute( name )
                    }
                } else {
                    var ns = isSvg && name !== ( name = name.replace( /^xlink:?/, "" ) )
                    if ( ns ) {
                        if ( nullOrFalse ) {
                            element.removeAttributeNS( XLINK_NS, name )
                        } else {
                            element.setAttributeNS( XLINK_NS, name, nextValue )
                        }
                    } else {
                        if ( nullOrFalse ) {
                            element.removeAttribute( name )
                        } else {
                            element.setAttribute( name, nextValue )
                        }
                    }
                }
            }
        }
    }

    createElement ( node, lifecycle, isSvg ) {
        const self: DOM = this

        const element =
            node.type === TEXT_NODE
                ? self.window.document.createTextNode( node.name )
                : ( isSvg = isSvg || node.name === "svg" )
                ? self.window.document.createElementNS( SVG_NS, node.name )
                : self.window.document.createElement( node.name )

        const props = node.props
        if ( props.oncreate ) {
            lifecycle.push( function () {
                props.oncreate( element )
            } )
        }

        for ( let i = 0, length = node.children.length; i < length; i++ ) {
            element.appendChild( self.createElement( node.children[ i ], lifecycle, isSvg ) )
        }

        for ( let name in props ) {
            self.updateProperty( element, name, null, props[ name ], isSvg )
        }

        return ( node.element = element )
    }

    removeChildren ( node ) {
        const self: DOM = this

        for ( let i = 0, length = node.children.length; i < length; i++ ) {
            self.removeChildren( node.children[ i ] )
        }

        const cb = node.props.ondestroy

        if ( cb != null ) {
            cb( node.element )
        }

        return node.element
    }

    removeElement ( parent, node ) {
        const self: DOM = this

        const remove = function () {
            parent.removeChild( self.removeChildren( node ) )
        }

        const cb = node.props && node.props.onremove

        if ( cb != null ) {
            cb( node.element, remove )
        } else {
            remove()
        }
    }

    patchElement ( parent, element, lastNode, nextNode, lifecycle, isSvg ) {
        const self = this

        if ( nextNode === lastNode ) {
        } else if (
            lastNode != null &&
            lastNode.type === TEXT_NODE &&
            nextNode.type === TEXT_NODE
        ) {
            if ( lastNode.name !== nextNode.name ) {
                element.nodeValue = nextNode.name
            }
        } else if ( lastNode == null || lastNode.name !== nextNode.name ) {
            const newElement = parent.insertBefore(
                self.createElement( nextNode, lifecycle, isSvg ),
                element
            )

            if ( lastNode != null ) self.removeElement( parent, lastNode )

            element = newElement
        } else {
            self.updateElement(
                element,
                lastNode.props,
                nextNode.props,
                lifecycle,
                ( isSvg = isSvg || nextNode.name === "svg" ),
                lastNode.type === RECYCLED_NODE
            )

            let savedNode
            let childNode

            let lastKey
            let lastChildren = lastNode.children
            let lastChStart = 0
            let lastChEnd = lastChildren.length - 1

            let nextKey
            let nextChildren = nextNode.children
            let nextChStart = 0
            let nextChEnd = nextChildren.length - 1

            while ( nextChStart <= nextChEnd && lastChStart <= lastChEnd ) {
                lastKey = getKey( lastChildren[ lastChStart ] )
                nextKey = getKey( nextChildren[ nextChStart ] )

                if ( lastKey == null || lastKey !== nextKey ) break

                self.patchElement(
                    element,
                    lastChildren[ lastChStart ].element,
                    lastChildren[ lastChStart ],
                    nextChildren[ nextChStart ],
                    lifecycle,
                    isSvg
                )

                lastChStart++
                nextChStart++
            }

            while ( nextChStart <= nextChEnd && lastChStart <= lastChEnd ) {
                lastKey = getKey( lastChildren[ lastChEnd ] )
                nextKey = getKey( nextChildren[ nextChEnd ] )

                if ( lastKey == null || lastKey !== nextKey ) break

                self.patchElement(
                    element,
                    lastChildren[ lastChEnd ].element,
                    lastChildren[ lastChEnd ],
                    nextChildren[ nextChEnd ],
                    lifecycle,
                    isSvg
                )

                lastChEnd--
                nextChEnd--
            }

            if ( lastChStart > lastChEnd ) {
                while ( nextChStart <= nextChEnd ) {
                    element.insertBefore(
                        self.createElement( nextChildren[ nextChStart++ ], lifecycle, isSvg ),
                        ( childNode = lastChildren[ lastChStart ] ) && childNode.element
                    )
                }
            } else if ( nextChStart > nextChEnd ) {
                while ( lastChStart <= lastChEnd ) {
                    self.removeElement( element, lastChildren[ lastChStart++ ] )
                }
            } else {
                const lastKeyed = createKeyMap( lastChildren, lastChStart, lastChEnd )
                const nextKeyed = {}

                while ( nextChStart <= nextChEnd ) {
                    lastKey = getKey( ( childNode = lastChildren[ lastChStart ] ) )
                    nextKey = getKey( nextChildren[ nextChStart ] )

                    if (
                        nextKeyed[ lastKey ] ||
                        ( nextKey != null && nextKey === getKey( lastChildren[ lastChStart + 1 ] ) )
                    ) {
                        if ( lastKey == null ) {
                            self.removeElement( element, childNode )
                        }
                        lastChStart++
                        continue
                    }

                    if ( nextKey == null || lastNode.type === RECYCLED_NODE ) {
                        if ( lastKey == null ) {
                            self.patchElement(
                                element,
                                childNode && childNode.element,
                                childNode,
                                nextChildren[ nextChStart ],
                                lifecycle,
                                isSvg
                            )
                            nextChStart++
                        }
                        lastChStart++
                    } else {
                        if ( lastKey === nextKey ) {
                            self.patchElement(
                                element,
                                childNode.element,
                                childNode,
                                nextChildren[ nextChStart ],
                                lifecycle,
                                isSvg
                            )
                            nextKeyed[ nextKey ] = true
                            lastChStart++
                        } else {
                            if ( ( savedNode = lastKeyed[ nextKey ] ) != null ) {
                                self.patchElement(
                                    element,
                                    element.insertBefore(
                                        savedNode.element,
                                        childNode && childNode.element
                                    ),
                                    savedNode,
                                    nextChildren[ nextChStart ],
                                    lifecycle,
                                    isSvg
                                )
                                nextKeyed[ nextKey ] = true
                            } else {
                                self.patchElement(
                                    element,
                                    childNode && childNode.element,
                                    null,
                                    nextChildren[ nextChStart ],
                                    lifecycle,
                                    isSvg
                                )
                            }
                        }
                        nextChStart++
                    }
                }

                while ( lastChStart <= lastChEnd ) {
                    if ( getKey( ( childNode = lastChildren[ lastChStart++ ] ) ) == null ) {
                        self.removeElement( element, childNode )
                    }
                }

                for ( let key in lastKeyed ) {
                    if ( nextKeyed[ key ] == null ) {
                        self.removeElement( element, lastKeyed[ key ] )
                    }
                }
            }
        }

        return ( nextNode.element = element )
    }

    patch ( lastNode, nextNode, container ) {
        const lifecycle = []

        this.patchElement(
            container, container.children[ 0 ], lastNode, nextNode, lifecycle, false )

        while ( lifecycle.length > 0 ) {
            lifecycle.pop()()
        }

        return nextNode
    }
}
