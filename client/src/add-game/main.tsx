import * as Effects from "../utils/effects"

import { createNode } from "../utils/html"

export const init = function () {
    return [
        {
            message: "Enter the Universal Bowling Code in order to fetch your data",
            game: null,
            ubc: "",
            valid: false
        },
        Effects.none
    ]
}

export const update = function ( message, state ) {
    let effect = Effects.none

    const { name, data } = message

    switch ( name ) {
        case "display-game":
            state = data.isError
                ? { ...state, message: data.data, game: null }
                : { ...state, game: data.data }
            break
        case "fetch-game":
            state = { ...state, ubc: data.ubc }
            effect = Effects.query( `/games/ubs/${ data.ubc }`, "display-game" )
            break
        case "display-response":
            state = { ...state, message: "Your game data as been added", game: null }
            break
        case "add-game":
            const game_to_add = state.game.players
                .find( player => player.name === data.name )
            effect = Effects.command( `/games/add-game`,
                { id: state.ubc, rolls: game_to_add.rolls, date: state.game.date },
                "display-response" )
            break
        case "verify-selection":
            state = { ...state, valid: data.name !== "" }
            break
    }

    return [ state, effect ]
}

export const view = function ( state ) {
    const playerView = player => <tr>
        <td><input type="radio" name="name" value={ player.name }/></td>
        <td>{ player.name }</td>
        <td>{ player.frames[ 9 ] }</td>
    </tr>

    const playerListView = game => <table>
        <thead>
            <th/>
            <th>Name</th>
            <th>Score</th>
        </thead>
        <tbody>{ game.players.map( playerView ) }</tbody>
    </table>

    const gameView = function ( state ) {
        if ( state.game ) {
            return <section id="game-view">
                <h3>{ state.game.date }</h3>
                <form onsubmit="add-game" oninput="verify-selection">
                    { playerListView( state.game ) }
                    <div>
                        <button disabled={ !state.valid }>Add Game</button>
                    </div>
                </form>
            </section>
        } else {
            return <section>
                <p>{ state.message }</p>
                <a href="../home.html">Home</a>
            </section>
        }
    }

    return <main>
        <h2>Add New Game</h2>
        <form onsubmit="fetch-game">
            <div>
                <label for="ubc-code">Code:</label>
                <input id="ubc-code" type="text" name="ubc" autocomplete="off"/>
                <button>Fetch Players</button>
            </div>
        </form>
        { gameView( state ) }
    </main>
}

