import HTTP from "../utils/http"

import * as Assertions from "./utils/assertions"

describe( "greeting", function () {
    const http = HTTP.withBaseURL( "http://localhost:8081" )

    it( "returns default greeting", function () {
        return http.get( "/greeting" )
            .then( Assertions.responseEquals( { text: "Hello, World!" } ) )
    } )

} )
