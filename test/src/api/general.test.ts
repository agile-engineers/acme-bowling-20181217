import HTTP from "../utils/http"

import * as Assertions from "./utils/assertions"

describe( "general", function () {
    const http = HTTP.withBaseURL( "http://localhost:8081" )

    it( "returns error response for unknown paths", function () {
        return http.get( "/unknown" ).then( Assertions.failsWith( 404 ) )
    } )

} )
