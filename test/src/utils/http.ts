import * as NativeHTTP from "http"
import * as URL from "url"

export default class HTTP {
    protocol: string
    hostname: string
    port: string

    private constructor ( baseUrl: string ) {
        const baseUrlParts = URL.parse( baseUrl )
        this.protocol = baseUrlParts.protocol
        this.hostname = baseUrlParts.hostname
        this.port = baseUrlParts.port
    }

    static withBaseURL ( url: string ): HTTP {
        return new HTTP( url )
    }

    private static createRequestHeaders ( token?: string ) {
        const headers = {
            "Content-Type": "application/json",
            "Accept": "application/json"
        }

        if ( token ) {
            headers[ "Authorization" ] = `Bearer ${ token }`
        }

        return headers
    }

    get ( path: string, token?: string ): Promise<any> {
        const options = {
            protocol: this.protocol,
            hostname: this.hostname,
            port: this.port,
            path: path,
            method: "GET",
            headers: HTTP.createRequestHeaders( token )
        }

        return new Promise( ( resolve, reject ) => {
            const req = NativeHTTP.request( options, function ( res ) {
                let response_data: string[] = []

                res.setEncoding( "utf8" )

                res.on( "data", function ( chunk: string ) {
                    response_data.push( chunk )
                } )

                res.on( "end", function () {
                    let body = undefined

                    try {
                        body = JSON.parse( response_data.join( "" ) )
                    } catch ( e ) {
                        // TODO: need to reject here or something
                    }

                    resolve( {
                        status: res.statusCode,
                        headers: res.headers,
                        body: body
                    } )
                } )
            } )

            req.on( "error", function ( err ) {
                reject( err )
            } )

            req.end()
        } )
    }

    post ( path: string, data: any, token?: string ): Promise<any> {
        const serialized_data = JSON.stringify( data )
        const request_headers = HTTP.createRequestHeaders( token )

        request_headers[ "Content-Length" ] = serialized_data.length

        const options = {
            protocol: this.protocol,
            hostname: this.hostname,
            port: this.port,
            path: path,
            method: "POST",
            headers: request_headers
        }

        return new Promise( ( resolve, reject ) => {
            const req = NativeHTTP.request( options, function ( res ) {
                let response_data: string[] = []

                res.setEncoding( "utf8" )

                res.on( "data", function ( chunk: string ) {
                    response_data.push( chunk )
                } )

                res.on( "end", function () {
                    let body = undefined

                    try {
                        body = JSON.parse( response_data.join( "" ) )
                    } catch ( e ) {
                        // TODO: need to reject here or something
                    }

                    resolve( {
                        status: res.statusCode,
                        headers: res.headers,
                        body: body
                    } )
                } )
            } )

            req.on( "error", function ( error ) {
                reject( error )
            } )

            req.write( serialized_data )
            req.end()
        } )
    }
}

// export const create = function ( url_string: string ) {
//     const { protocol, hostname, port } = URL.parse( url_string )
//
//     const createRequestHeaders = function ( token?: string ) {
//         const headers = {
//             "Content-Type": "application/json",
//             "Accept": "application/json"
//         }
//
//         if ( token ) {
//             headers[ "Authorization" ] = `Bearer ${ token }`
//         }
//
//         return headers
//     }
//
//     const get = function ( path: string, token?: string ): Promise<any> {
//         const options = {
//             protocol,
//             hostname,
//             port,
//             path: path,
//             method: "GET",
//             headers: createRequestHeaders( token )
//         }
//
//         return new Promise( ( resolve, reject ) => {
//             const req = HTTP.request( options, function ( res ) {
//                 let response_data: string[] = []
//
//                 res.setEncoding( "utf8" )
//
//                 res.on( "data", function ( chunk: string ) {
//                     response_data.push( chunk )
//                 } )
//
//                 res.on( "end", function () {
//                     let body = undefined
//
//                     try {
//                         body = JSON.parse( response_data.join( "" ) )
//                     } catch ( e ) {
//                         // TODO: need to reject here or something
//                     }
//
//                     resolve( {
//                         status: res.statusCode,
//                         headers: res.headers,
//                         body: body
//                     } )
//                 } )
//             } )
//
//             req.on( "error", function ( err ) {
//                 reject( err )
//             } )
//
//             req.end()
//         } )
//     }
//
//     const post = function ( path: string, data: any, token?: string ): Promise<any> {
//         const serialized_data = JSON.stringify( data )
//         const request_headers = createRequestHeaders( token )
//
//         request_headers[ "Content-Length" ] = serialized_data.length
//
//         const options = {
//             protocol: protocol,
//             hostname: hostname,
//             port: port,
//             path: path,
//             method: "POST",
//             headers: request_headers
//         }
//
//         return new Promise( ( resolve, reject ) => {
//             const req = HTTP.request( options, function ( res ) {
//                 let response_data: string[] = []
//
//                 res.setEncoding( "utf8" )
//
//                 res.on( "data", function ( chunk: string ) {
//                     response_data.push( chunk )
//                 } )
//
//                 res.on( "end", function () {
//                     let body = undefined
//
//                     try {
//                         body = JSON.parse( response_data.join( "" ) )
//                     } catch ( e ) {
//                         // TODO: need to reject here or something
//                     }
//
//                     resolve( {
//                         status: res.statusCode,
//                         headers: res.headers,
//                         body: body
//                     } )
//                 } )
//             } )
//
//             req.on( "error", function ( error ) {
//                 reject( error )
//             } )
//
//             req.write( serialized_data )
//             req.end()
//         } )
//     }
//
//     return { get, post }
// }
