import { assert } from "chai"

import * as Browser from "./utils/browser"
import * as Pages from "./utils/pages"

describe( "greeting page", function () {
    let page: Pages.GreetingPage

    before( Browser.startHeadless )
    after( Browser.stop )

    beforeEach( "navigate to greeting page", async function () {
        page = new Pages.GreetingPage( await this.browser.newPage() )
        await page.goto()
    } )

    afterEach( async function () {
        await page.close()
    } )

    it( "is accessible", async function () {
        assert.equal( await page.title(), "ACME" )
    } )

    it( "displays a default message", async function () {
        const message = await page.text( "#message" )

        assert.equal( message, "Hello, World!" )
    } )

} )

