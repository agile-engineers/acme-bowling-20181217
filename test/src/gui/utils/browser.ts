import * as Puppeteer from "puppeteer"

export const start = async function () {
    this.browser = await Puppeteer.launch( { headless: false, slowMo: 50 } )
}

export const startHeadless = async function () {
    this.browser = await Puppeteer.launch( { headless: true, slowMo: 0 } )
}

export const stop = async function () {
    await this.browser.close()
}
